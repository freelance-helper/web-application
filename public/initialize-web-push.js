firebase.initializeApp({
    messagingSenderId: '890406019265'
});
// браузер поддерживает уведомления
// вообще, эту проверку должна делать библиотека Firebase, но она этого не делает
const getToken = (messaging) => {
    messaging.requestPermission()
        .then(function () {
            // получаем ID устройства
            messaging.getToken()
                .then(function (currentToken) {
                    console.log(JSON.stringify({currentToken}));
                    if (currentToken) {
                        localStorage.setItem('MESSAGE_TOKEN', currentToken)
                        // sendTokenToServer(currentToken);
                    } else {
                        console.warn('Не удалось получить токен.');
                        // setTokenSentToServer(false);
                    }
                })
                .catch(function (err) {
                    console.warn('При получении токена произошла ошибка.', err);
                    // setTokenSentToServer(false);
                });
        })
        .catch(function (err) {
            console.warn('Не удалось получить разрешение на показ уведомлений.', err);
        });
};
if ('Notification' in window) {
    const messaging = firebase.messaging();
    if (window.Notification && Notification.permission !== "denied") {
        Notification.requestPermission((status) => {
            getToken(messaging);
            // status is "granted", if accepted by user
            /*const n = new Notification('Title', {
                body: 'I am the body text!',
                icon: '/path/to/icon.png' // optional
            })*/
        })
    }
    // пользователь уже разрешил получение уведомлений
    // подписываем на уведомления если ещё не подписали
    // по клику, запрашиваем у пользователя разрешение на уведомления
    // и подписываем его
    // $('#subscribe').on('click', function () {
    //     subscribe();
    // });
}
