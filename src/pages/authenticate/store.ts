import {combineReducers, Reducer} from 'redux';

import {loginReducer} from './features/login';
import {registrationReducer} from './features/registration';
import {IAuthenticateStore} from 'authenticate-types';

export const authenticateReducer: Reducer<IAuthenticateStore> = combineReducers({
    login: loginReducer,
    registration: registrationReducer,
});

