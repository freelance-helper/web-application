import React from 'react';
import styled from 'styled-components';
import {connect} from 'react-redux';

import {LoginForm} from './features/login';
import {RegistrationForm} from './features/registration';
import {
    registration,
    login,
    userInfoSelector,
} from './user-store';
import {IRootStore} from 'store-types';
import {
    IRegistrationCredentials,
    ILoginCredentials,
    IUser,
} from 'authenticate-types';
import {RouterProps} from "react-router";

const Wrapper = styled('div')`
  display: flex;
  height: 100vh;
`;

const Column = styled('div')`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 50%;
`;

const Header = styled('h2')``;

const mapStateToProps = (state: IRootStore) => ({
    user: userInfoSelector(state),
});

const mapDispatchToProps = {
    registration, login,
};

export const AuthenticatePageView: React.FC<IAuthenticatePageProps> =
    ({registration, login, user, history}) => {
        if (user.id) {
            history.push('/home')
        }

        return (
            <Wrapper>
                <Column>
                    <Header>Login</Header>
                    <LoginForm onSuccess={login}/>
                </Column>
                <Column>
                    <Header>Sign up</Header>
                    <RegistrationForm onSubmit={registration}/>
                </Column>
            </Wrapper>
        )
    };

export const AuthenticatePage = connect(mapStateToProps, mapDispatchToProps)(AuthenticatePageView);

interface IAuthenticatePageProps extends RouterProps {
    user: IUser,
    registration: (payload: IRegistrationCredentials) => void;
    login: (payload: ILoginCredentials) => void;
}
