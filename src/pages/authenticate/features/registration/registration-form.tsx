import React, {FormEvent, useState} from 'react';
import {connect} from 'react-redux';

import {registrationSelector, setRegistrationError, clearRegistrationError} from './store';
import {TextInput, InputField, Button, ButtonTheme} from 'ui';
import {InputWrapper} from '../../components/form';
import {IRegistrationCredentials} from 'authenticate-types';
import {IRootStore} from "store-types";

const mapStateToProps = (state: IRootStore) => ({
    registration: registrationSelector(state),
});

const mapDispatchToProps = {
    setRegistrationError,
    clearRegistrationError
};

export const RegistrationFormView: React.FC<IRegistrationFormProps> = ({onSubmit, registration, clearRegistrationError, setRegistrationError}) => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [retypedPassword, setRetypedPassword] = useState('');

    const handleChange = (handler: Function) => (value: string) => {
        registration.error && clearRegistrationError();
        handler(value);
    };

    const handleSubmit = (e: FormEvent) => {
        e.preventDefault();

        if (password === retypedPassword) {
            onSubmit({email, password})
        } else {
            setRegistrationError('Retype password correct')
        }
    };

    return (
        <form onSubmit={handleSubmit}>
            {registration.error && <span>{registration.message}</span>}
            <InputWrapper>
                <InputField
                    input={TextInput}
                    value={email}
                    name="registration-email"
                    onChange={handleChange(setEmail)}
                    label="Email"
                />
            </InputWrapper>
            <InputWrapper>
                <InputField
                    input={TextInput}
                    value={password}
                    name="registration-password"
                    onChange={handleChange(setPassword)}
                    label="Password"
                    type="password"
                />
            </InputWrapper>
            <InputWrapper>
                <InputField
                    input={TextInput}
                    value={retypedPassword}
                    name="retypedPassword"
                    onChange={handleChange(setRetypedPassword)}
                    label="Retype Password"
                    type="password"
                />
            </InputWrapper>
            <Button type="submit" theme={ButtonTheme.PRIMARY}>Registration</Button>
        </form>
    )
};

export const RegistrationForm = connect(mapStateToProps, mapDispatchToProps)(RegistrationFormView);

interface IRegistrationFormProps {
    setRegistrationError: (message: string) => void;
    clearRegistrationError: () => void;
    onSubmit: (payload: IRegistrationCredentials) => void;
    registration: IRegistrationStore,
}
