import {handleActions, createActions, combineActions} from 'redux-actions';

import {registration} from '../../user-store';
import {IRegistrationStore} from 'authenticate-types';
import {IRootStore} from "store-types";

const initialState: IRegistrationStore = {
    error: false,
    message: '',
};

export const {setRegistrationError, clearRegistrationError} = createActions({
    SET_REGISTRATION_ERROR: (message: string) => ({message, error: true}),
    CLEAR_REGISTRATION_ERROR: () => initialState,
});

export const registrationReducer = handleActions({
    [registration.toString()]: (state: IRegistrationStore, {error, payload}: any) => {
        if (error) {
            return {
                error: true,
                message: payload.response.data.message,
            }
        }

        return state;
    },
    [combineActions(setRegistrationError, clearRegistrationError).toString()]: (state: IRegistrationStore, {payload}) => payload,
}, initialState);

export const registrationSelector = (state: IRootStore) => state.authenticate.registration;
