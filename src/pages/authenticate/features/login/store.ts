import {combineActions, createActions, handleActions} from 'redux-actions';
import {login} from '../../user-store';
import {ILoginStore} from "authenticate-types";
import {IRootStore} from "store-types";

const initialStore: ILoginStore = {
    error: false,
    message: '',
};

export const {setLoginError, clearLoginError} = createActions({
    SET_LOGIN_ERROR: (message: string) => ({message, error: true}),
    CLEAR_LOGIN_ERROR: () => initialStore,
});

export const loginReducer = handleActions({
    [login.toString()]: (state: ILoginStore, {error, payload}: any) => {
        if (error) {
            return {
                error: true,
                message: payload.response.data.message,
            };
        }

        return state;
    },
    [combineActions(setLoginError, clearLoginError).toString()]: (state: ILoginStore, {payload}) => payload,
}, initialStore);

export const loginSelector = (state: IRootStore) => state.authenticate.login;
