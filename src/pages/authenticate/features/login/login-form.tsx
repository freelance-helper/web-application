import React, {FormEvent, useState} from 'react';
import {connect} from 'react-redux';

import {TextInput, InputField, Button, ButtonTheme} from 'ui';
import {ILoginCredentials, ILoginStore} from 'authenticate-types';
import {IRootStore} from "store-types";
import {loginSelector, clearLoginError} from './store';
import {InputWrapper} from '../../components/form';

const mapStateToProps = (state: IRootStore) => ({
    login: loginSelector(state)
});

const mapDispatchToProps = {
    clearLoginError
};

export const LoginFormView: React.FC<ILoginFormProps> = ({onSuccess, login, clearLoginError}) => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const handleChange = (handler: Function) => (value: string) => {
        login.error && clearLoginError();
        handler(value);
    };
    const handleSuccess = (e: FormEvent) => {
        e.preventDefault();
        onSuccess({email, password});
    };

    return (
        <form onSubmit={handleSuccess}>
            {login.error && <span>{login.message}</span>}
            <InputWrapper>
                <InputField
                    input={TextInput}
                    value={email}
                    name="login-email"
                    onChange={handleChange(setEmail)}
                    label="Email"
                />
            </InputWrapper>
            <InputWrapper>
                <InputField
                    input={TextInput}
                    value={password}
                    name="login-password"
                    onChange={handleChange(setPassword)}
                    label="Password"
                    type="password"
                />
            </InputWrapper>
            <Button type="submit" theme={ButtonTheme.PRIMARY}>Login</Button>
        </form>
    )
};

export const LoginForm = connect(mapStateToProps, mapDispatchToProps)(LoginFormView);

interface ILoginFormProps {
    clearLoginError: () => void;
    onSuccess: (payload: ILoginCredentials) => void;
    login: ILoginStore;
}
