import {ILoginCredentials, IRegistrationCredentials, IUserStore} from "authenticate-types";
import {combineActions, createActions, handleActions} from "redux-actions";

import {Api} from "api";
import {IRootStore} from "store-types";
import {getMessagesToken} from 'libs/get-messages-token';

export const USER_STORE_KEY = 'USER_STORE_KEY';

const initialUserStore: IUserStore = {
    info: {
        id: null,
        email: null,
    },
    authenticate: {
        accessToken: null,
        refreshToken: null,
        expiresIn: null,
    },
    messageToken: null,
    registration: false,
    notify: false,
};

const savedStore = localStorage.getItem(USER_STORE_KEY);
export enum NotifyStatus {
    ACTIVE = 'ACTIVE',
    INACTIVE = 'INACTIVE',
}

export const {
    login,
    registration,
    startNotification,
    stopNotification,
} = createActions({
    LOGIN: async (credentials: ILoginCredentials) => {
        const res = await Api.login(credentials);

        const {accessToken, refreshToken, expiresIn, user} = res.data;
        const {email, id, messageToken} = user;

        const data: IUserStore = {
            info: {email, id},
            authenticate: {accessToken, refreshToken, expiresIn},
            messageToken,
            notify: false,
        };

        localStorage.setItem(USER_STORE_KEY, JSON.stringify(data));

        if (messageToken) {
            return data;
        }

        const userId = res.data.user.id;
        const newMessageToken = await getMessagesToken();

        await Api.setMessageToken(newMessageToken, userId);

        return {
            ...data,
            messageToken: newMessageToken,
        };
    },
    REGISTRATION: async (credentials: IRegistrationCredentials) => {
        await Api.registration(credentials);

        return true;
    },
    START_NOTIFICATION: async (userId: number) => {
        await Api.post(`/users/${userId}/categories/status`, {
            status: NotifyStatus.ACTIVE,
        });

        return true
    },
    STOP_NOTIFICATION: async (userId: number) => {
        await Api.post(`/users/${userId}/categories/status`, {
            status: NotifyStatus.INACTIVE,
        });

        return false;
    },
});

export const userReducer = handleActions({
    [login.toString()]: (state: IUserStore, {error, payload}: any) => {
        if (error) {
            return state;
        }

        return payload;
    },
    [registration.toString()]: (state: IUserStore, {error, payload}: any) => {
        if (error) {
            return state;
        }

        return {...state, registration: payload};
    },
    [combineActions(startNotification, stopNotification).toString()]: (state: IUserStore, {error, payload}: any) => {
        return {
            ...state,
            notify: payload,
        };
    },
}, savedStore ? JSON.parse(savedStore) : initialUserStore);

export interface IUserLocalStorage {
    info: {
        id: number;
        email: string;
    },
    authenticate: {
        accessToken: string;
        refreshToken: string;
        expiresIn: number;
    },
    messageToken: string | null;
    notify: boolean;
}

export const userSelector = (state: IRootStore) => state.user;
export const userIdSelector = (state: IRootStore) => state.user.info.id;
export const userInfoSelector = (state: IRootStore) => userSelector(state).info;
export const isNotificationSelector = (state: IRootStore) => userSelector(state).notify;
export const userRegistrationSelector = (state: IRootStore) => userSelector(state).registration;
