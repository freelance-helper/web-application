import {handleActions} from 'redux-actions';

const initialStore: ISettingsStore = {};

export const settingsReducer = handleActions({}, initialStore);

export interface ISettingsStore {}
