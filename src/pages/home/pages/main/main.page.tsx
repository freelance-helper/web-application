import React from 'react';
import {connect} from 'react-redux';

import {IRootStore} from "store-types";
import {Button, ButtonTheme} from 'ui';
import {isNotificationSelector, startNotification, stopNotification,} from 'pages/authenticate/user-store';

const mapStoreToProps = (store: IRootStore) => ({
    isNotification: isNotificationSelector(store),
});

const mapDispatchToProps = {
    startNotification,
    stopNotification,
};

export const MainView: React.FC<IMainPageProps> = ({isNotification, stopNotification, startNotification}) => {
    const handleClick = () => {
        isNotification ? stopNotification() : startNotification();
    };

    const buttonTheme = isNotification ? ButtonTheme.ERROR : ButtonTheme.SUCCESS;
    const buttonText = isNotification ? 'Выключить уведомления' : 'Включить уведомления';

    return (
        <div>
            <Button
                onClick={handleClick}
                theme={buttonTheme}
            >{buttonText}</Button>
        </div>
    )
};

interface IMainPageProps {
    isNotification: boolean;
    startNotification: Function;
    stopNotification: Function;
}

export const MainPage = connect(mapStoreToProps, mapDispatchToProps)(MainView);
