import {Api} from 'api';
import {createActions, handleActions} from 'redux-actions';

import {IRootStore} from "store-types";

const initialState: ICategoriesStore = {
    exchanges: [],
    allCategories: [],
    userCategories: [],
    categoriesParseStatus: false,
};

export const {
    fetchExchanges,
    fetchUserCategories,
    fetchCategoriesByExchange,
    clearCategories,
    addUserCategory,
    removeUserCategory,
} = createActions({
    FETCH_EXCHANGES: async () => {
        const res = await Api.get('/exchanges');

        return res.data;
    },
    ADD_USER_CATEGORY: async (categoryId: number, userId: number) => {
        const res = await Api.post(`/users/${userId}/categories/${categoryId}`);

        return res.data.category;
    },
    REMOVE_USER_CATEGORY: async (categoryId: number, userId: number) => {
        await Api.delete(`/users/${userId}/categories/${categoryId}`);

        return categoryId;
    },
    FETCH_USER_CATEGORIES: async (userId: number) => {
        const res = await Api.get(`/users/${userId}/categories`);

        return res.data;
    },
    FETCH_CATEGORIES_BY_EXCHANGE: async (exchangeId: number) => {
        const res = await Api.get(`/categories/exchange/${exchangeId}/`);

        return res.data;
    },
    FETCH_ALL_CATEGORIES: async (userId: number) => {
        const res = await Api.get(`/users/${userId}/categories`);

        return res.data;
    },
    CLEAR_CATEGORIES: () => {},
});

export const categoriesReducer = handleActions({
    [addUserCategory.toString()]: (state: ICategoriesStore, {error, payload}: any) => {
        return {
            ...state,
            userCategories: [
                ...state.userCategories,
                payload
            ]
        };
    },
    [removeUserCategory.toString()]: (state: ICategoriesStore, {error, payload}: any) => {
        return {
            ...state,
            userCategories: state.userCategories.filter(category => category.id !== payload),
        };
    },
    [fetchUserCategories.toString()]: (state: ICategoriesStore, {error, payload}: any) => {
        return {
            ...state,
            userCategories: payload,
        };
    },
    [fetchExchanges.toString()]: (state: ICategoriesStore, {error, payload}: any) => {
        return {
            ...state,
            exchanges: payload,
        };
    },
    [fetchCategoriesByExchange.toString()]: (state: ICategoriesStore, {error, payload}: any) => {
        return {
            ...state,
            allCategories: payload,
        }
    },
    [clearCategories.toString()]: (state: ICategoriesStore) => {
        return {
            ...state,
            allCategories: [],
        }
    },
}, initialState);

export const exchangesSelector = (store: IRootStore) => store.categories.exchanges;
export const categoriesSelector = (store: IRootStore) => store.categories.allCategories;
export const userCategoriesSelector = (store: IRootStore) => store.categories.userCategories;

export interface ICategory {
    exchange: IExchange,
    children: ICategory[],
    name: string,
    url: string,
    id: number,
}

export interface IExchange {
    exchangeId: number,
    name: string,
    url: string,
}

export interface ICategoriesStore {
    exchanges: IExchange[],
    allCategories: any[],
    userCategories: ICategory[],
    categoriesParseStatus: boolean
}
