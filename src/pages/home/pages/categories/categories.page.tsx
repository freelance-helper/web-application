import React, {useEffect, useState} from 'react';
import {connect} from 'react-redux';
import {Route, RouteComponentProps} from 'react-router-dom';
import styled from 'styled-components';

import {
    fetchCategoriesByExchange,
    fetchExchanges,
    fetchUserCategories,
    exchangesSelector,
    userCategoriesSelector,
} from './store';
import {IExchange, IRootStore, ICategory} from 'store-types';
import {Breadcrumbs, breakpoints} from 'ui';
import {userIdSelector} from 'pages/authenticate/user-store';
import {ExchangesListPage} from './pages/exchages-list/exchages-list.page';
import {CategoriesSelectorPage} from './pages/categories-selector/categories-selector.page';

const Content = styled('div')`
  display: flex;
`;

const Card = styled('div')`
  margin-left: auto;
  background-color: white;
  min-width: 300px;
  min-height: 60vh;
  padding: 20px;
  box-shadow: 0 1px 8px -3px rgba(0,0,0,0.50);
  
  ul {
    padding: 0;
    list-style: none;
    
    li {
      display: flex;
      flex-direction: column;
      padding: 10px 20px;
      margin-bottom: 15px;
      background-color: white;
      border-radius: 5px;
      box-shadow: 0 1px 8px -3px rgba(0,0,0,0.50);
      
      span {
        margin-top: 5px;
        font-size: 12px;
      }
    }
  }
  
  @media (max-width: ${breakpoints.md}px) {
    display: none;
  }
`;

const mapDispatchToProps = {
    fetchCategoriesByExchange,
    fetchUserCategories,
    fetchExchanges
};

const mapStoreToProps = (store: IRootStore) => ({
    userId: userIdSelector(store),
    exchanges: exchangesSelector(store),
    userCategories: userCategoriesSelector(store),
});

export const CategoriesView: React.FC<ICategoriesPageProps> = (
    {fetchCategoriesByExchange, userId, exchanges, userCategories, fetchUserCategories, fetchExchanges, history}
) => {
    const [exchange, setExchange] = useState<IExchange | null>(null);

    useEffect(() => {
        fetchExchanges();
        fetchUserCategories(userId);
    }, []);

    const handleExchangeClick = async (exchange: IExchange) => {
        await fetchCategoriesByExchange(exchange.exchangeId);
        setExchange(exchange);
        history.push(`/home/categories/${exchange.exchangeId}/selector`);
    };

    const handleExchangeSelectClick = () => {
        setExchange(null);
        history.push('/home/categories/exchanges');
    };

    return (
        <Content>
            <div>
                <Breadcrumbs>
                    <Breadcrumbs.Item onClick={handleExchangeSelectClick}>Выбрать биржу</Breadcrumbs.Item>
                    {exchange && (
                        <Breadcrumbs.Item>{exchange.name}</Breadcrumbs.Item>
                    )}
                </Breadcrumbs>
                <Route path="/home/categories/exchanges"
                       component={() => (
                           <ExchangesListPage
                               exchanges={exchanges}
                               onExchangeClick={handleExchangeClick}
                           />
                       )}/>
                <Route path="/home/categories/:exchangeId/selector"
                       component={CategoriesSelectorPage}/>
            </div>
            <Card>
                <h3>Ваши категории</h3>
                <ul>
                    {userCategories.map(category => (
                        <li key={category.id}>
                            {category.name}
                            <span>{category.exchange.name}</span>
                        </li>
                    ))}
                </ul>
            </Card>
        </Content>
    )
};

export const CategoriesPage = connect(mapStoreToProps, mapDispatchToProps)(CategoriesView);

interface ICategoriesPageProps extends RouteComponentProps {
    fetchCategoriesByExchange: Function;
    fetchUserCategories: Function;
    exchanges: IExchange[],
    userCategories: ICategory[],
    fetchExchanges: Function;
    userId: number | null,
}
