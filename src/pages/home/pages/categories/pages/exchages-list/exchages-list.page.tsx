import React from 'react';
import styled from 'styled-components';

import {IExchange} from 'store-types';
import {breakpoints} from "../../../../../../ui/theme";

const List = styled('ul')`
  display: flex;
  flex-wrap: wrap;
  list-style: none;
  padding: 0;
  margin-right: 0;
  font-size: 17px;
`;

const Item = styled('li')`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 6px 20px;
  margin-top: 20px;
  margin-right: 20px;
  border-radius: 5px;
  background-color: white;
  box-shadow: 0 1px 8px -3px rgba(0,0,0,0.50);
  cursor: pointer;
  
  span {
    margin-top: 4px;
    font-size: 12px;
  }
  
  @media (max-width: ${breakpoints.xs}px) {
    width: 100%;
    margin-right: 0;
  }
`;

export const ExchangesListPage: React.FC<IExchangesPageProps> = ({exchanges, onExchangeClick}) => (
    <>
        <List>
            {exchanges.map((item: IExchange) => (
                <Item key={item.exchangeId} onClick={() => onExchangeClick(item)}>
                    {item.name}
                    <span>{item.url}</span>
                </Item>
            ))}
        </List>
    </>
);

interface IExchangesPageProps {
    exchanges: IExchange;
    onExchangeClick: (exchange: IExchange) => void;
}
