import React, {useEffect, useState} from 'react';
import styled from "styled-components";
import {connect} from 'react-redux';
import {RouteComponentProps} from "react-router";

import {ICategory, IRootStore} from "store-types";
import {
    categoriesSelector,
    userCategoriesSelector,
    addUserCategory,
    removeUserCategory,
    fetchCategoriesByExchange
} from '../../store';
import {userIdSelector} from 'pages/authenticate/user-store';

const Row = styled('div')`
  display: flex;
`;

const Column = styled('ul')`
  display: inline-flex;
  flex-direction: column;
  padding: 0 20px 0 0;
  margin-right: 20px;
  border-right: 1px solid var(--border-color);
  list-style: none;
  
  &:last-child {
    border-right-color: transparent;
  }
`;

const Item = styled('li')`
  position: relative;
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 10px 46px 10px 20px;
  margin-bottom: 15px;
  background-color: white;
  border-radius: 5px;
  box-shadow: 0 1px 8px -3px rgba(0,0,0,0.50);
  cursor: pointer;
  
  svg {
    position: absolute;
    right: 10px;
    top: calc(50% - 8px);
    margin-left: 15px;
  }
`;

const mapStoreToProps = (store: IRootStore) => ({
    categories: categoriesSelector(store),
    userCategories: userCategoriesSelector(store),
    userId: userIdSelector(store),
});

const mapDispatchToProps = {
    fetchCategoriesByExchange,
    addUserCategory,
    removeUserCategory,
};

export const CategoriesSelectorView: React.FC<ICategoriesSelectorPageProps> = (
    {
        categories,
        userCategories,
        userId,
        fetchCategoriesByExchange,
        addUserCategory,
        removeUserCategory,
        match
    }
) => {
    const [categoriesPath, setCategoriesPath] = useState<ICategory[]>([]);

    useEffect(() => {
        fetchCategoriesByExchange(Number(match.params.exchangeId));
    }, []);

    const isUserCategory = (category: ICategory) => userCategories
        .map(item => item.id)
        .includes(category.id);

    const getCategoryIconType = (category: ICategory): string => {
        if (category.children.length === 0) {
            return isUserCategory(category) ? 'delete' : 'add';
        }

        if (!categoriesPath[0]) {
            return 'none';
        }

        if (categoriesPath[0].id === category.id) {
            return 'active';
        }

        return 'none';
    };

    const handleCategoryClick = (category: ICategory) => () => {
        if (category.children.length !== 0) {
            return setCategoriesPath([category]);
        }

        isUserCategory(category) ?
            removeUserCategory(category.id, userId) :
            addUserCategory(category.id, userId);
    };

    return (
        <Row>
            <Column>
                {categories.map(category => (
                    <CategoryItem
                        key={category.id}
                        category={category}
                        onClick={handleCategoryClick(category)}
                        type={getCategoryIconType(category)}
                    />
                ))}
            </Column>
            {categoriesPath.map(column => (
                <Column key={column.id}>
                    {column.children.map((category: ICategory) => (
                        <CategoryItem
                            key={category.id}
                            category={category}
                            onClick={handleCategoryClick(category)}
                            type={getCategoryIconType(category)}
                        />
                    ))}
                </Column>
            ))}
        </Row>
    )
};

const CategoryItem: React.FC<{ category: ICategory, onClick: () => void, type: string }> = ({category, onClick, type}) => (
    <Item onClick={onClick}>
        {category.name}
        {type === 'active' && (
            <svg width="16" height="16" xmlns="http://www.w3.org/2000/svg">
                <path stroke="#979797" d="M7 1l7.1 7-7 7.1" fill="none" fillRule="evenodd"/>
            </svg>
        )}
        {type === 'add' && (
            <svg width="16" height="16" version="1" xmlns="http://www.w3.org/2000/svg">
                <g fill="none" fillRule="evenodd" stroke="#979797">
                    <path d="M8 0v16M0 8h16"/>
                </g>
            </svg>
        )}
        {type === 'delete' && (
            <svg width="16" height="16" version="1" xmlns="http://www.w3.org/2000/svg">
                <g stroke="#979797" fill="none" fillRule="evenodd">
                    <path d="M13 3L3 13M3 3l10 10"/>
                </g>
            </svg>
        )}
    </Item>
);

interface ICategoriesSelectorPageProps extends RouteComponentProps<{ exchangeId: string }> {
    userId: number | null,
    categories: ICategory[];
    userCategories: ICategory[];
    fetchCategoriesByExchange: Function;
    addUserCategory: Function,
    removeUserCategory: Function,
}

export const CategoriesSelectorPage = connect(mapStoreToProps, mapDispatchToProps)(CategoriesSelectorView);
