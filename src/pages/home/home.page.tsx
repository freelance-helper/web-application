import React, {useState} from 'react';
import {connect} from 'react-redux';
import {Switch, Redirect, RouteComponentProps} from 'react-router-dom';

import {MainTemplate} from 'ui';
import {CategoryIcon, SettingsIcon, HomeIcon} from 'assets/icons';
import {userInfoSelector} from 'pages/authenticate';
import {IRootStore} from "store-types";
import {INavItem} from "home-types";
import {Header, Aside} from './features';
import {pageTitleSelector, Page} from 'features/page';
import {MainPage} from './pages/main/main.page';
import {SettingsPage} from './pages/settings/settings.page';
import {CategoriesPage} from './pages/categories/categories.page';

const mapStateToProps = (state: IRootStore) => ({
    email: userInfoSelector(state).email,
    title: pageTitleSelector(state),
});

export const HomeView: React.FC<IHomePageProps> = ({email, title, history}) => {
    const [activeNav, setActiveNav] = useState(getDefaultKey());

    const handleNavClick = (item: INavItem) => {
        setActiveNav(item.key);
        history.push(item.payload.path);
    };

    return (
        <MainTemplate
            title={title}
            header={<Header email={email}/>}
            aside={
                <Aside
                    navigationItems={navigationItems}
                    onNavClick={handleNavClick}
                    activeNav={activeNav}/>
            }
            content={
                <Switch>
                    <Page title="Главная" path="/home/main" component={MainPage}/>
                    <Page title="Категории" path="/home/categories" component={CategoriesPage}/>
                    <Page title="Настройки" path="/home/settings" component={SettingsPage}/>
                    <Redirect exact from="/home" to="/home/main"/>
                </Switch>
            }
        />
    )
};

const getDefaultKey = () => navigationItems.reduce((acc: string, item: INavItem) => (
    window.location.href.includes(item.key) ? item.key : acc
), navigationItems[0].key);

const navigationItems: INavItem[] = [
    {
        title: 'Главная',
        key: 'main',
        icon: HomeIcon,
        payload: {path: '/home/main'}
    },
    {
        title: 'Категории',
        key: 'categories',
        icon: CategoryIcon,
        payload: {path: '/home/categories/exchanges'}
    },
    {
        title: 'Настройки',
        key: 'settings',
        icon: SettingsIcon,
        payload: {path: '/home/settings'}
    },
];

export const HomePage = connect(mapStateToProps)(HomeView as any);

interface IHomePageProps extends RouteComponentProps {
    email: string;
    title: string;
}
