import React from 'react';
import styled from 'styled-components';

const HeaderWrapper = styled('header')`
  display: flex;
  justify-content: space-between;
  align-items: center;
  height: 100%;
  padding: 10px;
  box-sizing: border-box;
`;

const Logo = styled('img')`
  width: 100%;
  height: 100%;
  max-width: 30px;
  max-height: 30px;
  background-color: gray;
  border-radius: 50%;
`;

const Email = styled('p')`
  padding: 0;
  margin: 0;
  font-size: .9rem;
  font-weight: bold;
`;

export const Header: React.FC<IHeaderProps> = ({email}) => (
    <HeaderWrapper>
        <Logo/>
        <Email>{email}</Email>
    </HeaderWrapper>
);

interface IHeaderProps {
    email: string;
}
