import React from 'react';
import styled from 'styled-components';

import {breakpoints, palette} from 'ui';

const AsideWrapper = styled('aside')`
  height: 100%;
  padding: 0 10px;
`;

const Nav = styled('nav')`
  height: 100%;
`;

const NavItem = styled('li')<{active: boolean}>`
  display: flex;
  align-items: center;
  height: 100%;
  padding: 10px 0;
  cursor: pointer;
  ${p => p.active && `color: ${palette.coral}`}
  
  svg {
    width: 20px;
    height: 20px;
  }
  
  span {
    margin-left: 7px;
  }
`;

const NavList = styled('ul')`
  list-style: none;
  padding: 0;
  margin: 0;
  
  @media (max-width: ${breakpoints.sm}px) {
    display: flex;
    justify-content: space-between;
    height: 100%;
    
    svg {
      width: 25px;
      height: 25px;
    }
    
    ${NavItem} {
      flex-direction: column;
      justify-content: center;
      align-items: center;
      font-size: 10px;
      padding: 0;
      
      span {
        margin-left: 0;
        margin-top: 4px;
      }
    }
  }
`;

export const Aside: React.FC<IAsideProps> = ({navigationItems = [], onNavClick, activeNav}) => (
    <AsideWrapper>
        <Nav>
            <NavList>
                {navigationItems.map((item: INavItem) => (
                    <NavItem
                        active={activeNav === item.key}
                        onClick={() => onNavClick(item)}
                        key={item.key}
                    >
                        <item.icon/>
                        <span>{item.title}</span>
                    </NavItem>
                ))}
            </NavList>
        </Nav>
    </AsideWrapper>
);

interface IAsideProps {
    navigationItems: INavItem[];
    onNavClick: (item: INavItem) => void;
    activeNav: string;
}

export interface INavItem {
    title: string;
    key: string;
    payload?: any;
    icon: React.FC;
}
