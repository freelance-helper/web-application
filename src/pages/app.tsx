import React from 'react';
import {Route, Redirect, Switch} from 'react-router-dom';

import {PrivateRoute} from 'features/private-route';
import {AuthenticatePage} from 'pages/authenticate';
import {HomePage} from 'pages/home';

export const App: React.FC = () => {
  return (
      <Switch>
          <Route path="/authenticate" component={AuthenticatePage}/>
          <PrivateRoute path="/home" component={HomePage}/>
          <Redirect exact from="/" to="/home"/>
      </Switch>
  );
};
