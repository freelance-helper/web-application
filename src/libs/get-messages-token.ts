const firebase = (window as any).firebase;
const Notification = (window as any).Notification;

firebase.initializeApp({
    messagingSenderId: '890406019265'
});

export const getMessagesToken = async () => {
    if (Notification) {
        const messaging = firebase.messaging();
        await messaging.requestPermission();
        return messaging.getToken();
    } else {
        throw new Error('Браузер не поддерживает уведомления');
    }
};

/*if (permission !== NotificationStatus.DENIED) {
            const status = await Notification.requestPermission();

            if (status === NotificationStatus.GRANTED) {
                getToken(messaging);
            }
        } else {
            getToken(messaging);
        }*/

// пользователь уже разрешил получение уведомлений
// подписываем на уведомления если ещё не подписали
// по клику, запрашиваем у пользователя разрешение на уведомления
// и подписываем его
// $('#subscribe').on('click', function () {
//     subscribe();
// });

// status is "granted", if accepted by user
/*const n = new Notification('Title', {
    body: 'I am the body text!',
    icon: '/path/to/icon.png' // optional
})*/
