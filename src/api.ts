import axios, {AxiosPromise, AxiosRequestConfig} from 'axios';

import {config} from 'config';
import {USER_STORE_KEY, IUserLocalStorage} from 'pages/authenticate/user-store';
import {IRegistrationCredentials, ILoginCredentials} from 'authenticate-types';

const axiosInstance = axios.create({
    baseURL: config.apiHost,
});

const getAuthenticatedConfig = (requestConfig: AxiosRequestConfig, userData: IUserLocalStorage): AxiosRequestConfig => {
    return {
        ...requestConfig,
        headers: {
            ...requestConfig.headers,
            'Authorization': `Bearer ${userData.authenticate.accessToken}`,
            'x-user-id': userData.info.id,
        }
    }
};

axiosInstance.interceptors.request.use(async (requestConfig: AxiosRequestConfig) => {
    const localStorageData = localStorage.getItem(USER_STORE_KEY);

    if (!localStorageData) {
        return requestConfig;
    }

    const userData: IUserLocalStorage = JSON.parse(localStorageData);

    if (Date.now() < userData.authenticate.expiresIn * 1000) {
        return getAuthenticatedConfig(requestConfig, userData);
    }

    try {
        const res = await axios.get(config.apiHost + `/authenticate/refresh-token/${userData.authenticate.refreshToken}`);
        const updatedAuthenticateData: IUserLocalStorage = {...userData, authenticate: res.data};
        localStorage.setItem(USER_STORE_KEY, JSON.stringify(updatedAuthenticateData));

        return getAuthenticatedConfig(requestConfig, updatedAuthenticateData);
    } catch (e) {
        localStorage.removeItem(USER_STORE_KEY);
        window.location.replace('/#/authenticate');
        return requestConfig;
        // throw new Error('Authentication time is over')
    }
});

export class Api {
    private static async formatResponse(res: AxiosPromise): Promise<ApiResponse> {
        const {status, data} = await res;

        return {status, data}
    }

    static async registration(payload: IRegistrationCredentials) {
        return this.formatResponse(
            axios.post(`${config.apiHost}/authenticate/registration`, payload)
        );
    }

    static async login(payload: ILoginCredentials) {
        return this.formatResponse(
            axios.post(`${config.apiHost}/authenticate/login`, payload)
        )
    }

    static async setMessageToken(messageToken: string, userId: number) {
        return this.put(`/users/${userId}/message-token`, {messageToken});
    }

    static async get(url: string) {
        return this.formatResponse(
            axiosInstance.get(url)
        )
    }

    static async post(url: string, body?: any) {
        return this.formatResponse(
            axiosInstance.post(url, body)
        )
    }

    static async put(url: string, body?: any) {
        return this.formatResponse((
            axiosInstance.put(url, body)
        ))
    }

    static async delete(url: string) {
        return this.formatResponse(
            axiosInstance.delete(url)
        )
    }
}

interface ApiResponse {
    status: number;
    data: any;
}

