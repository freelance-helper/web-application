import React, {Fragment} from 'react';
import ReactDOM from 'react-dom';
import {HashRouter as Router} from "react-router-dom";
import {Provider as ReduxProvider} from 'react-redux';

import {App} from './pages/app';
import {GlobalStyles} from 'ui/theme';
import {register as registerServiceWorker} from './libs/service-worker';
import {configureStore} from "store";

const store = configureStore();

ReactDOM.render(
    <Fragment>
        <GlobalStyles/>
        <ReduxProvider store={store}>
            <Router>
                <App/>
            </Router>
        </ReduxProvider>
    </Fragment>,
    document.getElementById('root')
);

registerServiceWorker();
