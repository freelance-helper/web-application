import {camelToKebab} from 'libs/camel-to-kebab';
import {palette} from './palette';

const c = {
    primary: '#FF5964',
    success: palette.green,
    warning: palette.gold,
    error: palette.coral,
    borderColor: '#979797',
    mainBackground: '#F9FAFB',
    mainText: '#414042',
    transparent: 'transparent',
    white: '#fff',
};

type Colors = typeof c;
export const colors: Colors = c;

export const colorsCss = Object.keys(colors).reduce((acc, key) => `${acc}--${camelToKebab(key)}:${(colors as any)[key]};`, '');
