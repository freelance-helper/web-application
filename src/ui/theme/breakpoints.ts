export const breakpoints = {
    xs: 400,
    sm: 700,
    md: 1000,
    lg: 1300,
    xl: 1600,
};
