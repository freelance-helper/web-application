import React, {useRef, useEffect, useState} from 'react';
import styled from 'styled-components';

import {getWindowWidth} from 'libs/get-window-width';
import {breakpoints} from '../theme';

const Header = styled('header')<{visible: boolean}>`
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 16px;
  font-weight: 700;
  width: 100%;
  height: 100%;
  border-bottom: 1px solid ${p => p.visible ? 'var(--border-color)' : 'transparent'};
  
  span {
    opacity: ${p => p.visible ? '1' : '0'};
  }
`;

const Wrapper = styled('main')`
  display: grid;
  grid-template-areas: 
    "header header"
    "aside content";
  grid-template-rows: 60px 1fr;
  grid-template-columns: .2fr 1fr;
  min-height: 100vh;
  background-color: var(--main-background);
  
  @media (max-width: ${breakpoints.sm}px) {
    grid-template-areas: 
      "header"
      "content"
      "aside"
    ;
    grid-template-columns: 1fr;
    grid-template-rows: 50px 1fr 60px;
    min-height: 100%;
  }
`;

const HeaderContainer = styled('div')`
  grid-area: header;
  border-bottom: 1px solid var(--border-color);
  
  @media (max-width: ${breakpoints.sm}px) {
    border-bottom: none;
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: var(--header-fixed-height);
    z-index: 10;
  }
`;

const AsideContainer = styled('div')`
  padding-top: 20px;
  padding-left: 15%;
  padding-right: 20px;
  grid-area: aside;
  border-right: 1px solid var(--border-color);
  box-sizing: border-box;
  
  @media (max-width: ${breakpoints.sm}px) {
    position: fixed;
    bottom: 0;
    left: 0;
    z-index: 10;
    width: 100%;
    height: var(--bottom-bar-fixed-height);
    border-right: none;
    border-top: .7px solid var(--border-color);
    padding: 0 20px;
    background-color: var(--white);
  }
`;

const ContentContainer = styled('main')`
  padding-top: 20px;
  padding-left: 20px;
  padding-right: 20px;
  grid-area: content;
  overflow-y: scroll;
  box-sizing: border-box;
  
  @media (max-width: ${breakpoints.sm}px) {
    padding-top: 0;
    max-height: calc(100vh - var(--bottom-bar-fixed-height) - var(--header-fixed-height));
    
    h2 {
      margin-top: 0;
    }
  }
`;

const getIsMobile = () => {
    return getWindowWidth() < breakpoints.sm;
};

export const MainTemplate: React.FC<IMainTemplateProps> = ({header, aside, content, title}) => {
    const contentElement = useRef<HTMLDivElement>(null);
    const [isMobile, setIsMobile] = useState(getIsMobile());
    const [isSmallTitle, setIsSmallTitle] = useState(false);

    const handleScroll = () => {
        if (contentElement.current === null) {
            return;
        }

        if (contentElement.current.scrollTop > 27) {
            return setIsSmallTitle(true)
        }

        if (contentElement.current.scrollTop < 27) {
            return setIsSmallTitle(false)
        }
    };
    const handleWindowWidthChange = () => {
        setIsMobile(getIsMobile());
    };

    useEffect(() => {
        if (contentElement === null || contentElement.current === null) {
            return;
        }

        window.addEventListener('resize', handleWindowWidthChange);
        contentElement.current.addEventListener('scroll', handleScroll);

        return () => {
            window.removeEventListener('resize', handleWindowWidthChange);
            if (contentElement.current !== null) {
                contentElement.current.removeEventListener('scroll', handleScroll);
            }
        }
    }, []);

    const getHeader = () => {
        if (!isMobile) {
            return header;
        }

        return (
            <Header visible={isSmallTitle}>
                <span>{title}</span>
            </Header>
        )
    };

    return (
        <Wrapper>
            <HeaderContainer>{getHeader()}</HeaderContainer>
            <ContentContainer ref={contentElement}>
                <h2>{title}</h2>
                {content}
            </ContentContainer>
            <AsideContainer>{aside}</AsideContainer>
        </Wrapper>
    )
};

interface IMainTemplateProps {
    title: string;
    header: JSX.Element;
    aside: JSX.Element;
    content: JSX.Element;
}
