import React from 'react';
import styled from 'styled-components';

const BreadcrumbsList = styled('ul')`
  display: flex;
  padding: 0;
  list-style: none;
`;

const BreadcrumbsItem = styled('li')`
  position: relative;
  padding-right: 15px;
  margin-right: 15px;
  font-weight: bold;
  cursor: pointer;
  
  &::after {
    content: '/';
    position: absolute;
    right: 0;
    top: 50%;
    transform: translateY(-50%);
  }
  
  &:last-child {
    font-weight: normal;
   
   &:after {
    display: none;
   }
  }
`;

export const Breadcrumbs: IBreadcrumbs = ({children, ...props}) => (
    <BreadcrumbsList {...props}>
        {children}
    </BreadcrumbsList>
);

interface IBreadcrumbs extends React.FC {
    Item: React.FunctionComponent<React.HTMLAttributes<HTMLLIElement>>;
}

Breadcrumbs.Item = BreadcrumbsItem;
