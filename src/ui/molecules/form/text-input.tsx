import React from 'react';
import styled from 'styled-components';
import {palette} from "ui/theme";

import {IInputProps} from './input-props';

const Input = styled('input')`
  border-radius: 4px;
  border: .5px solid ${palette.graphite60};
`;

export const TextInput: React.FC<IInputProps> = ({value, name, id = name, onChange, type = 'text'}) => (
    <Input
        id={id}
        type={type}
        name={name}
        value={value}
        onChange={(event) => onChange(event.target.value, name)}
    />
);
