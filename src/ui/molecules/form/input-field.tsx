import React from 'react';
import styled from 'styled-components';

import {IInputProps} from './input-props';

const Wrapper = styled('div')`
  display: flex;
  flex-direction: column;
`;

export const InputField: React.FC<IInputFieldProps> = (
    {name, id = name, value, onChange, label, type, input: Input}
) => (
    <Wrapper>
        <label htmlFor={id}>{label}</label>
        <Input id={id} name={name} value={value} onChange={onChange} type={type}/>
    </Wrapper>
);

interface IInputFieldProps extends IInputProps {
    label: string;
    input: React.FC<IInputProps>,
}
