export interface IInputProps {
    value: string;
    name: string;
    id?: string;
    onChange: (value: string, name: string) => void;
    type?: string;
}
