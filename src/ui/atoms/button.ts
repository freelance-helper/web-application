import styled from 'styled-components';
import {colors} from 'ui/theme/colors';

export enum ButtonTheme {
    SUCCESS = 'SUCCESS',
    WARNING = 'WARNING',
    ERROR = 'ERROR',
    PRIMARY = 'PRIMARY',
    DEFAULT = 'DEFAULT',
}

const getButtonTheme = (type: ButtonTheme) => {
    switch (type) {
        case ButtonTheme.DEFAULT:
            return {
                color: colors.mainText,
                background: colors.transparent,
            };
        case ButtonTheme.SUCCESS:
            return {
                color: colors.white,
                background: colors.success,
            };
        case ButtonTheme.ERROR:
            return {
                color: colors.white,
                background: colors.error,
            };
        default:
            return {
                color: colors.mainText,
                background: colors.transparent,
            }
    }
};

export const Button = styled('button')<IButtonProps>`
  background: ${(p: IButtonProps) => getButtonTheme(p.theme).background};
  color: ${(p: IButtonProps) => getButtonTheme(p.theme).color};
  padding: 10px 20px;
  border-radius: 3px;
  border: none;
  cursor: pointer;
  outline: none;
`;

type IButtonProps = {
    theme: ButtonTheme;
    type?: string;
}
