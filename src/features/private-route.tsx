import React, {FunctionComponent} from 'react';
import {Route, Redirect} from 'react-router-dom';
import {ConnectedComponentClass} from "react-redux";

import {USER_STORE_KEY} from 'pages/authenticate/user-store';

export const PrivateRoute: React.FC<IPrivateRouteProps> = ({component: Component, ...props}) => {
    const data = localStorage.getItem(USER_STORE_KEY);

    return (
        <Route
            {...props}
            render={props =>
                data ?
                    <Component {...props}/> :
                    <Redirect to={{
                        pathname: '/authenticate',
                        state: { from: props.location }
                    }}/>
            }
        />
    )
};

interface IPrivateRouteProps {
    component: FunctionComponent<any> | ConnectedComponentClass<any, any>;
    path: string;
}
