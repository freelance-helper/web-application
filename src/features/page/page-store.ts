import {createActions, handleActions} from 'redux-actions';
import {IRootStore} from "store-types";

const initialState: IPageStore = {
    title: '',
};

export const {setPageTitle} = createActions({
    SET_PAGE_TITLE: (title: string) => title,
});

export const pageReducer = handleActions({
    [setPageTitle.toString()]: (store: IPageStore, {payload}: any) => {
        return {
            ...store,
            title: payload,
        };
    },
}, initialState);

export const pageTitleSelector = (store: IRootStore) => store.page.title;

export interface IPageStore {
    title: string;
}
