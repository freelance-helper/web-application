import React, {useEffect} from 'react';
import {connect} from 'react-redux';
import {Route, RouteComponentProps} from 'react-router-dom';

import {setPageTitle} from './page-store';

const mapDispatchToProps = {
    setPageTitle
};

export const PageView: React.FC<IPageProps> = ({title, path, component, setPageTitle}) => {
    useEffect(() => {
        setPageTitle(title);
    }, [path]);

    return (
        <Route path={path} component={component}/>
    )
};

interface IPageProps {
    title: string;
    path: string;
    component: React.ComponentType<RouteComponentProps<any>> | React.ComponentType<any>;
    setPageTitle: Function;
}

export const Page = connect(null, mapDispatchToProps)(PageView);
