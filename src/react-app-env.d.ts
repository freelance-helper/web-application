/// <reference types="react-scripts" />

interface IAuthenticateStore {
    login: ILoginStore;
    registration: IRegistrationStore;
}

interface ILoginStore {
    error: boolean;
    message: string;
}

interface IRegistrationStore {
    error: boolean;
    message: string;
}

interface IAuthenticateData {
    accessToken: string | null;
    refreshToken: string | null;
    expiresIn: number | null;
}

interface IUser {
    id: number | null;
    email: string | null;
}

interface IUserStore {
    info: IUser;
    authenticate: IAuthenticateData;
    messageToken: string | null;
    registration?: boolean;
    notify: boolean;
}

declare module 'store-types' {
    import {ICategoriesStore} from './pages/home/pages/categories/store';
    import {IExchange, ICategory} from './pages/home/pages/categories/store';
    import {IPageStore} from './features/page/page-store';
    import {ISettingsStore} from './pages/home/pages/settings/settings-store';

    export interface IRootStore {
        authenticate: IAuthenticateStore;
        user: IUserStore;
        categories: ICategoriesStore;
        page: IPageStore;
        settings: ISettingsStore;
    }

    export {IExchange};
    export {ICategory};
}

declare module 'authenticate-types' {
    export interface IRegistrationCredentials {
        email: string;
        password: string;
    }

    export interface ILoginCredentials {
        email: string;
        password: string;
    }

    export {IAuthenticateStore}

    export {ILoginStore}

    export {IUserStore}

    export {IRegistrationStore}

    export {IAuthenticateData}

    export {IUser}
}

declare module 'home-types' {
    import {INavItem} from 'pages/home/features/aside';

    export {INavItem};
}
