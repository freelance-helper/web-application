import React from 'react';

export const CategoryIcon: React.FC<{fill?: string}> = ({fill = 'currentColor'}) => (
    <svg width="16" height="16" viewBox="0 0 16 16">
        <g fill="none" fillRule="evenodd" stroke={fill}>
            <g strokeWidth=".7">
                <path d="M2 1c-.7 0-1 .3-1 .8V6M1 6c0 .7.3 1 .8 1H6M6 7c.7 0 1-.3 1-.9V2M7 2c0-.7-.3-1-.8-1H2"/>
            </g>
            <g strokeWidth=".5">
                <path d="M2 9c-.7 0-1 .3-1 .8V14M1 14c0 .7.3 1 .8 1H6M6 15c.7 0 1-.3 1-.9V10M7 10c0-.7-.3-1-.8-1H2"/>
            </g>
            <g strokeWidth=".5">
                <path d="M10 1c-.7 0-1 .3-1 .8V6M9 6c0 .7.3 1 .8 1H14M14 7c.7 0 1-.3 1-.9V2M15 2c0-.7-.3-1-.8-1H10"/>
            </g>
            <g strokeWidth=".7">
                <path
                    d="M10 9c-.7 0-1 .3-1 .8V14M9 14c0 .7.3 1 .8 1H14M14 15c.7 0 1-.3 1-.9V10M15 10c0-.7-.3-1-.8-1H10"/>
            </g>
        </g>
    </svg>
);
