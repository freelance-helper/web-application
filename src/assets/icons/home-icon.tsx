import React from 'react';

export const HomeIcon: React.FC<{fill?: string}> = ({fill = 'currentColor'}) => (
    <svg width="16" height="16" viewBox="0 0 16 16">
        <g stroke={fill} strokeWidth=".7" fill="none" fillRule="evenodd">
            <g strokeLinecap="round">
                <path d="M8 1.5l6 5M8 1.5l-6 5"/>
            </g>
            <g strokeLinecap="square" strokeLinejoin="round">
                <path d="M8 14.1H3v-7M8 14.1h5v-7"/>
            </g>
        </g>
    </svg>
);
