import React from 'react';

export const SettingsIcon: React.FC<{fill?: string}> = ({fill = 'currentColor'}) => (
    <svg width="16" height="16" viewBox="0 0 16 16">
        <path
            d="M5 13.2a6 6 0 0 1-1-.7l-1.5.7c-.4-.4-.8-1-1-1.5l1-1.2a6 6 0 0 1-.4-1.3L.6 9a7.8 7.8 0 0 1 0-2L2 6.9l.5-1.3-1.2-1.2 1.1-1.5 1.5.7a6 6 0 0 1 1-.7L4.9 1 6.6.5 7.4 2a6 6 0 0 1 1.2 0L9.4.5l1.8.6-.3 1.7 1.1.7 1.5-.7c.4.4.8 1 1 1.5l-1 1.2.4 1.3 1.5.3a7.9 7.9 0 0 1 0 1.9l-1.5.2a6 6 0 0 1-.5 1.3l1.2 1.2c-.3.6-.7 1-1.1 1.5l-1.5-.7a6 6 0 0 1-1 .7l.2 1.7-1.8.6-.8-1.5a6 6 0 0 1-1.2 0l-.8 1.5-1.8-.6.3-1.7zm3-.7a4.5 4.5 0 1 0 0-9 4.5 4.5 0 0 0 0 9z"
            stroke={fill} strokeWidth=".5" fill="none" fillRule="evenodd"/>
    </svg>
);
