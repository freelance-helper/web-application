import {combineReducers, createStore, compose, applyMiddleware, Reducer} from 'redux';
import { createLogger } from 'redux-logger';
import promiseMiddleware from 'redux-promise';

import {IRootStore} from 'store-types';
import {authenticateReducer, userReducer} from 'pages/authenticate';
import {categoriesReducer} from 'pages/home/pages/categories/store';
import {settingsReducer} from 'pages/home/pages/settings/settings-store';
import {pageReducer} from 'features/page/page-store';

const reducer: Reducer<IRootStore> = combineReducers({
    authenticate: authenticateReducer,
    user: userReducer,
    categories: categoriesReducer,
    page: pageReducer,
    settings: settingsReducer,
});

const composeEnhancers = (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const configureStore = (initialStore = {}) => {
    const middleware = [
        createLogger({ collapsed: true }),
        promiseMiddleware,
    ];

    return createStore(
        reducer,
        initialStore,
        composeEnhancers(applyMiddleware(...middleware))
    )
};

